const newData = `{
    "ip": "181.139.192.133",
    "last_entry_date": "1/3/2021",
    "entry_count": "1",
    "data": {
        "ip": "181.139.192.133",
        "is_eu": false,
        "city": "Medellín",
        "region": "Antioquia",
        "region_code": "ANT",
        "country_name": "Colombia",
        "country_code": "CO",
        "continent_name": "South America",
        "continent_code": "SA",
        "latitude": "6.2059",
        "longitude": "-75.5901",
        "postal": "050022",
        "calling_code": "57",
        "flag": "https://ipdata.co/flags/co.png",
        "emoji_flag": "🇨🇴",
        "emoji_unicode": "U+1F1E8 U+1F1F4",
        "asn": {
            "asn": "AS13489",
            "name": "EPM Telecomunicaciones S.A. E.S.P.",
            "domain": "tigo.com.co",
            "route": "181.128.0.0/12",
            "type": "isp"
        },
        "languages": [
            {
                "name": "Spanish",
                "native": "Español"
            }
        ],
        "currency": {
            "name": "Colombian Peso",
            "code": "COP",
            "symbol": "CO$",
            "native": "$",
            "plural": "Colombian pesos"
        },
        "time_zone": {
            "name": "America/Bogota",
            "abbr": "-05",
            "offset": "-0500",
            "is_dst": false,
            "current_time": "2021-03-01T12:43:25.312201-05:00"
        },
        "threat": {
            "is_tor": false,
            "is_proxy": false,
            "is_anonymous": false,
            "is_known_attacker": false,
            "is_known_abuser": false,
            "is_threat": false,
            "is_bogon": false
        },
        "count": "4"
    },
    "__v": "0"
}
`
module.exports =  JSON.parse(newData)