const Entry = require('../models/entry.model');

function createEntry(req) {
  const entry = new Entry(req.body);
  entry.save().then(result => {
    if (result) {
      return ('Entry Created Successfully!');
    }
  })
  return null;
}
function updateEntry(req, result) {
  result.ip = req.body.ip;
  result.last_entry_date = req.body.last_entry_date;
  result.entry_count = req.body.entry_count;
  result.data = req.body.data;
  result.save().then(result2 => {
    if (result2) {
      return ('Entry Update Successfully!');
    }
  })
  return null;
}

exports.create = async (req, res) => {
  Entry.findById(req.body._id, (err, result) => {
    if (result) {
      res.json ('The Entry Already Exists in the DB')
    } else {
      res.json(createEntry(req))
    }
  });
};

exports.update = async (req, res) => {
  Entry.findById(req.params.id, (err, result) => {
    if (!result) {
      res.json('There is no entry with that ID');
    } else {
      res.json(updateEntry(req, result))
    }
  });
};

exports.findAll = async (req, res) => {
  Entry.find(function (err, result) {
    if (result) {
      res.json(result);
    }
  });
};

exports.findIp = async (req, res) => {
  Entry.findOne(function (err, result) {
    if (result) {
      res.json(result);
    }
  }).where('ip').equals(req.params.ip)
};
