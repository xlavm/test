const mongoose = require('mongoose')
const Schema = mongoose.Schema

const EntrySchema = new Schema({
    ip: {
        type: String
    },
    last_entry_date: {
        type: String
    },
    entry_count: {
        type: Number
    },
    data: {
        type: JSON
    }
}, {
    collection: 'gitlab-ci-cd-collection'
});

module.exports = mongoose.model('entry', EntrySchema);