# !/bin/bash

ssh $SERVER_USER@$SERVER_IP "docker stop $CONTAINER_NAME_PROD >> /dev/null 2>&1 && docker rm $CONTAINER_NAME_PROD >> /dev/null 2>&1"
status=$?
if [ $status -ne 0 ]; then
    echo "Not exist the container but will creating"
fi

ssh $SERVER_USER@$SERVER_IP docker run -d --name $CONTAINER_NAME_PROD -p $PORT_EXPOSE_PROD:$PORT_API_PROD $CI_REGISTRY/$PROJECT_PATH:latest
