# CI/CD

this project for CI/CD process 

## Branches Flow 

![branches-flow](images/branches-flow.png)
`Note:` in this moment I don't approvals for merge request

## GitLab Branches Config
Go to: `Settings Project > Repository > Protected branches`

![branches-config](images/branches-config.png)

## Pipeline Flow DEV 

![pipeline-flow-dev](images/pipeline-flow-dev.png)

## Pipeline Flow PROD

![pipeline-flow-prod](images/pipeline-flow-prod.png)

## GitLab CI/CD Config

![gitlab-ci-cd-variables](images/gitlab-ci-cd-variables.png)

## Environments PROD and DEV

* Production: http://52.21.102.224:4000/ 
* Development: http://52.21.102.224:3000/

## Principal Commands

Install Dependencies
```
npm install
```
build api
```
npm build
```
Execute in PROD
```
npm start
```
Execute in DEV
```
npm run start:dev
```

test API
```
npm test
```

## Project Local Config 
.env file 
```
PORT=3000
PATH_MONGO=mongodb+srv://manager-apis-back-user:dyldMQYT7HV*&HwEtRc7W!H@manager-apis-back-clust.z1hax.mongodb.net/manager-apis-back-db?retryWrites=true&w=majority
PATH_API_ENTRY=/api/v1/entry
```