# !/bin/bash

ssh $SERVER_USER@$SERVER_IP "docker stop $CONTAINER_NAME_DEV >> /dev/null 2>&1 && docker rm $CONTAINER_NAME_DEV >> /dev/null 2>&1"
status=$?
if [ $status -ne 0 ]; then
    echo "Not exist the container but will creating"
fi

ssh $SERVER_USER@$SERVER_IP docker run -d --name $CONTAINER_NAME_DEV -p $PORT_EXPOSE_DEV:$PORT_API_DEV $CI_REGISTRY/$PROJECT_PATH:latest
